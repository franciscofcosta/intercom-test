/**
 * @function sorter - Sorts an array of objects
 * @param {Object} config - Config object for sorter function
 * @param {Array} config.array - Array of objects to sort
 * @param {Object} config.key - Object with key property, to serve as sorting criteria
 * @param {String} config.key.key - Key to sort array with
 * @param {String} config.order - Sorting order, 'ASC' or 'DESC'. Defaults to 'ASC'
 */
const sorter = ({ array, key, order = 'ASC' }) => {
  // Check for errors in arguments
  checkErrors({ array, key, order });

  // Sort array
  const sortedArray = array.sort(function (a, b) {
    return order === 'DESC' ? b[key] - a[key] : a[key] - b[key];
  });

  return sortedArray;
};

const checkErrors = ({ array, key, order }) => {
  if (!Array.isArray(array)) {
    throw new Error({ message: 'Objects to sort must be in array' });
  }

  if (!key) {
    throw new Error({ message: 'Must provide a sort key' });
  }

  const elementWithoutKey = array.find((element) => !element[key]);
  if (elementWithoutKey) {
    throw new Error({
      message: 'Sort key must be property of all objects in array to sort.',
    });
  }

  if (order && !(order === 'ASC' || order === 'DESC')) {
    throw new Error({ message: 'Must provide a valid sort order.' });
  }
};

module.exports = sorter;
