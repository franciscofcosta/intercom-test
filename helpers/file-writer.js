const fs = require('fs');

/**
 * @function writeToFile - Creates an output file and writes one line separated JSON formatted lines for each element of input array
 * @param {Array} array - Array to be written to output file.
 */
const writeToFile = (array) => {
  if (array.length === 0) {
    return {
      message: 'No data to write.',
    };
  }
  const file = fs.createWriteStream('output.txt');
  return array.forEach(function (v) {
    file.write(JSON.stringify(v) + '\n');
  });
};

module.exports = writeToFile;
