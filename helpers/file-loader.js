const fs = require('fs');

/**
 * @function convertTxtToJSONArray - Function that takes a txt file with JSON lines and converts to array
 * @param {Object} file - Object with path property and value for relative path of file to read
 */
const convertTxtToJSONArray = (file) => {
  const convertedArray = fs.readFileSync(file.path).toString().split('\n');
  return convertedArray.map((element) => JSON.parse(element));
};

module.exports = convertTxtToJSONArray;
