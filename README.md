# Intercom Software Engineer Test

This is my solution to the [Intercom Software Engineer Test](exercise.md).

## Structure and Functionality

I have used Node.js to build the solution for the exercise.
The entry point of the program is the `index.js` file, which is run by the start script.

The program uses a service `DistanceCalculator`, which is responsible for running all tasks related to calculating the distance between two points on a sphere. This service implements the [_great-circle_ formula](https://en.wikipedia.org/wiki/Great-circle_distance#Radius_for_spherical_Earth) required by the exercise.

The program also uses a series of helper functions, in the `helpers` directory, to run assisting tasks like file loading, sorting and writing outputs.

The output of the program is an `output.txt` file with the solution to the exercise.

### On distance calculation

On the topic of the great circle distance, I have decided to use only the arccosine formula and not the haversine formula. According to Wikipedia, the haversine formula is better suited to measure small distances with precision. However, on further investigation I have found that the simple arccosine formula "gives well-condi­tioned results down to distances as small as a few metres" in [modern Javascript 64-point floating numbers](https://www.movable-type.co.uk/scripts/latlong.html). Therefore, for the current exercise, I assumed the simple formula would be enough as a difference as small as 0.001 degrees in coordinates (+- 120 meters) between any customer and the Dublin office would already be calculated with precision. Nevertheless, I have built the `DistanceCalculator` as a service that to which other service functions can be added, such as the haversine formula to calculate small distances.

## Installation

- Open your terminal and clone this repo by running `git clone https://gitlab.com/franciscofcosta/intercom-test.git`.
- Get into the directory by running `cd intercom-test`.
- Install the project's dependencies by running `yarn install` or `npm install`, depending on your package manager.

## Usage

- Run the project with the command `yarn start` or `npm start`.
- Once the program finishes running, a file named `output.txt` will be created in the project's root directory, with the output of the program.

## Running tests

This project uses [Jest](https://jestjs.io/) for testing.
To run the test suite, run `yarn test` in your terminal, from the project root directory.
To get code coverage reporting, run `yarn test --coverage` in your terminal, from the project root directory.
