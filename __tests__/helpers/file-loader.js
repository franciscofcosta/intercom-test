const fileLoader = require('../../helpers/file-loader');

describe('File Loader', () => {
  test('loads file and converts to JSON array', () => {
    const convertedArray = fileLoader({
      path: './__tests__/data/mockCustomers.txt',
    });
    expect(convertedArray).toEqual([
      {
        latitude: 'testLat',
        user_id: 12,
        name: 'name1',
        longitude: 'testLong',
      },
      {
        latitude: 'testLat2',
        user_id: 1,
        name: 'name2',
        longitude: 'testLong2',
      },
    ]);
  });
});
