const fs = require('fs');
const writeToFile = require('../../helpers/file-writer');

describe('Filte Writer', () => {
  beforeAll(() => {
    jest.clearAllMocks();
  });
  test('creates output file', () => {});
  const mockFs = jest.spyOn(fs, 'createWriteStream');
  describe('With empty array', () => {
    const testArray = [];
    test("doesn't create output file with empty array", () => {
      writeToFile(testArray);
      expect(mockFs).not.toHaveBeenCalled();
    });
    test('returns "no data" message', () => {
      const response = writeToFile(testArray);
      expect(response).toEqual({ message: 'No data to write.' });
    });
  });
  describe('Without empty array', () => {
    const testArray = [
      { name: 'Sarah A', user_id: 1 },
      { name: 'John B', user_id: 2 },
    ];
    test('creates output file', () => {
      mockFs.mockReturnValue({
        write: jest.fn().mockReturnValue({ message: 'File written' }),
      });
      writeToFile(testArray);
      expect(mockFs).toHaveBeenCalled();
    });
    test('writes to file', () => {
      mockFs.mockReturnValue({
        write: jest.fn().mockReturnValue({ message: 'File written' }),
      });
      writeToFile(testArray);
      expect(mockFs().write).toHaveBeenCalled();
      expect(mockFs().write).toHaveBeenCalledWith(
        JSON.stringify({ name: 'Sarah A', user_id: 1 }) + '\n'
      );
      expect(mockFs().write).toHaveBeenCalledWith(
        JSON.stringify({ name: 'John B', user_id: 2 }) + '\n'
      );
    });
  });
});
