const sorter = require('../../helpers/sorter');

describe('Sorter', () => {
  describe('With incorrect arguments', () => {
    test('should throw error if array to sort is not of type Array', () => {
      const config = { array: { test: 'test' } };
      expect(() => sorter(config)).toThrowError(
        new Error({ message: 'Objects to sort must be in array' })
      );
    });
    test('should throw error if no sort key is provided', () => {
      const config = { array: [{ test: 'test', id: 1 }] };
      expect(() => sorter(config)).toThrowError(
        new Error({ message: 'Must provide a sort key' })
      );
    });
    test('should throw error if provided key is not in object to sort', () => {
      const config = {
        array: [
          { test: 'test', id: 1 },
          { test: 'test2', id: 2 },
        ],
        key: 'age',
      };
      expect(() => sorter(config)).toThrowError(
        new Error({
          message: 'Sort key must be property of all objects in array to sort.',
        })
      );
    });
  });
  describe('With correct arguments', () => {
    const config = {
      array: [
        { name: 'John', age: 30 },
        { name: 'Jane', age: 32 },
        { name: 'Adam', age: '16' },
        { name: 'Sarah', age: 23 },
      ],
      key: 'age',
    };

    test('should sort array correctly with provided key as criteria', () => {
      const sorted = sorter(config);
      expect(sorted).toEqual([
        { name: 'Adam', age: '16' },
        { name: 'Sarah', age: 23 },
        { name: 'John', age: 30 },
        { name: 'Jane', age: 32 },
      ]);
    });

    test('should sort ascending if no sort order is provided', () => {
      const sorted = sorter({ ...config, order: 'DESC' });
      expect(sorted).toEqual([
        { name: 'Jane', age: 32 },
        { name: 'John', age: 30 },
        { name: 'Sarah', age: 23 },
        { name: 'Adam', age: '16' },
      ]);
    });
  });
});
