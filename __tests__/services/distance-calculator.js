const DistanceCalculator = require('../../services/distance-calculator');

describe('DistanceCalculator', () => {
  beforeAll(() => {
    jest.clearAllMocks();
  });

  describe('Service', () => {
    const A = { latitude: '53.339428', longitude: '-6.257664' };
    const B = { latitude: '53.2451022', longitude: '-6.238335' };
    const distanceCalculator = new DistanceCalculator(A, B);
    test('service has correct methods and properties', () => {
      expect(distanceCalculator.calculateDistance).toBeTruthy();
      expect(distanceCalculator.calculateLongDistance).toBeTruthy();
      expect(distanceCalculator.convertDegreesToRadians).toBeTruthy();
      expect(distanceCalculator.pointA).toBeTruthy();
    });
  });

  describe('Calculate distance', () => {
    describe('without 2 sets of coordinates', () => {
      const A = { latitude: '53.339428', longitude: '-6.257664' };
      test('returns error message', () => {
        // Instantiate distanceCalculator service with only one set of coordinates

        const distanceCalculator = new DistanceCalculator(A);
        // Expect to throw error message
        expect(() => {
          distanceCalculator.calculateDistance();
        }).toThrowError(
          new Error({
            message: 'Missing coordinate object.',
          })
        );
      });
    });

    describe('with 2 sets of coordinates', () => {
      test('converts degrees to radians correctly', () => {
        const A1Coords = { latitude: '1', longitude: '1' };
        const B = { latitude: '53.2451022', longitude: '-6.238335' };
        const distanceCalculator = new DistanceCalculator(A1Coords, B);
        const AInRadians = distanceCalculator.convertDegreesToRadians(A1Coords);
        expect(AInRadians).toBeInstanceOf(Object);
        expect(AInRadians.latitude).toBe(Math.PI / 180);
        expect(AInRadians.longitude).toBe(Math.PI / 180);
      });

      describe('with same set of coordinates', () => {
        const A = { latitude: '53.2451022', longitude: '-6.238335' };
        const B = { latitude: '53.2451022', longitude: '-6.238335' };
        const distanceCalculator = new DistanceCalculator(A, B);

        test('distance function is not called', () => {
          distanceCalculator.calculateDistance();
          const spyCalculateLongDistance = jest.spyOn(
            distanceCalculator,
            'calculateLongDistance'
          );
          expect(spyCalculateLongDistance).not.toHaveBeenCalled();
        });
        test('returns object with distance 0', () => {
          const distance = distanceCalculator.calculateDistance();
          expect(distance).toEqual({ pointA: A, pointB: B, distance: 0 });
        });
      });

      describe('with different set of coordinates', () => {
        describe('with long distance', () => {
          const A = { latitude: '38.736946', longitude: '-9.142685' };
          const B = { latitude: '51.509865', longitude: '-0.118092' };
          const distanceCalculator = new DistanceCalculator(A, B);

          test('runs converter function', () => {
            const spyConvertRadians = jest.spyOn(
              distanceCalculator,
              'convertDegreesToRadians'
            );
            distanceCalculator.calculateDistance();
            expect(spyConvertRadians).toHaveBeenCalled();
          });

          test('runs arccos formula to calculate distance', () => {
            const spyCalculateLongDistance = jest.spyOn(
              distanceCalculator,
              'calculateLongDistance'
            );
            distanceCalculator.calculateDistance();
            expect(spyCalculateLongDistance).toHaveBeenCalled();
          });

          test('calculates distance correctly', () => {
            const distance = distanceCalculator.calculateDistance();
            expect(Math.round(distance.distance)).toEqual(1584);
          });

          test('returns object with two sets of coordinates and distance between them', () => {
            const distance = distanceCalculator.calculateDistance();
            expect(distance).toEqual({
              pointA: distanceCalculator.pointA,
              pointB: distanceCalculator.pointB,
              distance: distance.distance,
            });
          });
        });
      });
    });
  });
});
