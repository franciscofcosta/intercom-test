/**
 * DistanceCalculator - Service that applies great circle formulaes to calculate the shortest distance between two points on the surface of a sphere
 * @param {Object} A - Object with latitude and longitude, in degrees, both strings.
 * @param {Object} B - Object with latitude and longitude, in degrees, both strings.
 */
function DistanceCalculator(A, B) {
  this.pointA = A;
  this.pointB = B;
}

/**
 * @function calculateLongDistance - Function that applies the great circle distance between two points, assuming the mean radius of the Earth of 6371km.
 * @param {Object} A - Object with latitude, longitude, in radians, both strings.
 * @param {Object} B - Object with latitude, longitude, in radians, both strings.
 * @returns {Object} - Object with pointA, pointB as coordinates for two points on a sphere, in degrees, both strings; and distance as distance between pointA and pointB, in kilometers.
 */
DistanceCalculator.prototype.calculateLongDistance = function (A, B) {
  const longDifference = A.longitude - B.longitude;
  const latDifference = A.latitude - B.latitude;

  if (longDifference === latDifference && latDifference === 0) {
    return { pointA: this.pointA, pointB: this.pointB, distance: 0 };
  }

  const centralAngle = Math.acos(
    Math.sin(A.latitude) * Math.sin(B.latitude) +
      Math.cos(A.latitude) * Math.cos(B.latitude) * Math.cos(longDifference)
  );

  const earthRadius = 6371;
  const distance = earthRadius * centralAngle;
  return { pointA: this.pointA, pointB: this.pointB, distance };
};

/**
 * @function convertDegreesToRadians - Function to convert degrees to radians
 * @param {Object} degreeCoords - Object with longitude, latitude, in degrees, both strings.
 * @returns {Object} - Object with longitude, latitude, in radians, both strings.
 */
DistanceCalculator.prototype.convertDegreesToRadians = function (degreeCoords) {
  const radianLatitude = degreeCoords.latitude * (Math.PI / 180);
  const radianLongitude = degreeCoords.longitude * (Math.PI / 180);
  return { latitude: radianLatitude, longitude: radianLongitude };
};

/**
 * @function calculateDistance - Function
 */
DistanceCalculator.prototype.calculateDistance = function () {
  if (!this.pointA || !this.pointB) {
    throw new Error({ message: 'Missing coordinate object.' });
  }
  const pointARadians = this.convertDegreesToRadians(this.pointA);
  const pointBRadians = this.convertDegreesToRadians(this.pointB);
  return this.calculateLongDistance(pointARadians, pointBRadians);
};

module.exports = DistanceCalculator;
