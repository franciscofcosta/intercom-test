// Services and helpers
const fileLoader = require('./helpers/file-loader');
const fileWriter = require('./helpers/file-writer');
const sorter = require('./helpers/sorter');
const DistanceCalculator = require('./services/distance-calculator');

// Data
const customers = fileLoader({ path: './data/customers.txt' });
const targetLocation = require('./data/office.json');

// Calculate distance to office for each customer
console.log('Calculating distances...');
const customersWithDistance = customers.map((customer) => {
  const distanceCalculator = new DistanceCalculator(targetLocation, {
    latitude: customer.latitude,
    longitude: customer.longitude,
  });
  const distance = distanceCalculator.calculateDistance();
  return { ...customer, distanceToOffice: distance.distance };
});

// Filter customers closer than 100km from the office
console.log('Filtering customers...');
const customersToInvite = customersWithDistance.filter(
  (customer) => customer.distanceToOffice <= 100
);

// Sort customers and clean data
const sortedCustomers = sorter({ array: customersToInvite, key: 'user_id' });
const finalList = sortedCustomers.map((customer) => {
  return {
    name: customer.name,
    user_id: customer.user_id,
  };
});

// Write output file
console.log('Writing output file...');
fileWriter(finalList);

console.log('Done');
